﻿using Kontakt.Mapper;
using Kontakt.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kontakt.Service
{
    public class KontaktService
    {
        private static KontaktService kontaktService = null;
        public static KontaktService getSingleton()
        {
            if (kontaktService == null)
            {
                kontaktService = new KontaktService();
            }
            return kontaktService;
        }


        public KontaktE getById(int id)
        {
            var kontaktMapper = new KontaktMapper();
            var kontakt = kontaktMapper.getById(id);
            var konatktTelefonMapper = new KontaktTelefonMapper();
            var kontaktTelefon = konatktTelefonMapper.getKontaktTelefonList(id);
            kontakt.telefon = kontaktTelefon;
            return kontakt;
        }


        public List<KontaktE> getList()
        {
            var kontaktMapper = new KontaktMapper();
            var listaKontakt = kontaktMapper.getList();
            return listaKontakt;
        }

        public KontaktTelefon getTelefonById(int id)
        {
            var kontaktTelefonMapper = new KontaktTelefonMapper();
            var kontaktTelefon = kontaktTelefonMapper.getTelefonById(id);
            return kontaktTelefon;
        }


        public List<KontaktTelefon> getTelefonList(int id)
        {
            var kontaktTelefonMapper = new KontaktTelefonMapper();
            var listaKontaktTelefon = kontaktTelefonMapper.getKontaktTelefonList(id);
            return listaKontaktTelefon;
        }


    }
}