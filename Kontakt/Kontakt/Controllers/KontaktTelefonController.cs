﻿using Kontakt.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kontakt.Controllers
{
    public class KontaktTelefonController : Controller
    {
        // GET: KontaktTelefon
        public ActionResult KontaktTelefon(int id)
        {
            var kontaktTelefonService = new KontaktService();
            var kontaktTelefon = kontaktTelefonService.getTelefonById(id);
            return View(kontaktTelefon);
        }


        public ActionResult ListaKontaktTelefon(int id)
        {
            var kontaktTelefonService = new KontaktService();
            var listaKontaktTelefon = kontaktTelefonService.getTelefonList(id);
            return View(listaKontaktTelefon);
        }
    }
}