﻿using Kontakt.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kontakt.Controllers
{
    public class KontaktController : Controller
    {
        // GET: Kontakt
        public ActionResult Kontakt(int id)
        {
            var kontaktService = new KontaktService();
            var kontakt = kontaktService.getById(id);
            return View(kontakt);
        }


        public ActionResult listaKontakt()
        {
            var kontaktService = new KontaktService();
            var listaKontakt = kontaktService.getList();
            return View(listaKontakt);
        }
    }
}