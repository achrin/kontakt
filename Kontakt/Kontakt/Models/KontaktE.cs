﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kontakt.Models
{
    public class KontaktE
    {
        public int id { get; set; } = 0;
        public string imie { get; set; } = string.Empty;
        public string nazwisko { get; set; } = string.Empty;
        public int wiek { get; set; } = 0;
        public List<KontaktTelefon> telefon { get; set; } = new List<KontaktTelefon>();
    }
}