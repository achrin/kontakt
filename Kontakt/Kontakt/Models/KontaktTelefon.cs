﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kontakt.Models
{
    public class KontaktTelefon
    {
        public int id { get; set; } = 0;
        public long numerTelefon { get; set; } = 0;
        public int idKontakt { get; set; } = 0;
    }
}