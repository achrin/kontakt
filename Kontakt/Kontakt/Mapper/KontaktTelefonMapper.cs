﻿using Kontakt.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Kontakt.Mapper
{
    public class KontaktTelefonMapper
    {
        public List<KontaktTelefon> getKontaktTelefonList(int id)
        {
            var listaKontaktTelefon = new List<KontaktTelefon>();
            using (var conn = new SqlConnection())
            {
                conn.ConnectionString = "Data Source = 192.168.10.248; Initial Catalog = Agata; User ID = agata; Password = pbs1234";
                conn.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = conn;
                    command.CommandText = $"select * from kontakt_telefon where id_kontakt = {id}";
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            listaKontaktTelefon.Add(new KontaktTelefon()
                            {
                                id = (int)reader["id"],
                                numerTelefon = (long)reader["numer"],
                                idKontakt = (int)reader["id_kontakt"]
                            });
                        }
                    }
                }
            }
                return listaKontaktTelefon;
        }

        public KontaktTelefon getTelefonById(int id)
        {
            var kontaktTelefon = new KontaktTelefon();
            using (var conn = new SqlConnection())
            {
                conn.ConnectionString = "Data Source = 192.168.10.248; Initial Catalog = Agata; User ID = agata; Password = pbs1234";
                conn.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = conn;
                    command.CommandText = $"select id, numer from kontakt_telefon where id_kontakt = {id}";
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            kontaktTelefon.id = (int)reader["id"];
                            kontaktTelefon.numerTelefon = (long)reader["numer"];
                            kontaktTelefon.idKontakt = (int)reader["id_kontakt"];
                        }
                    }
                }
            }
            return kontaktTelefon;
        }
    }
}