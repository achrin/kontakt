﻿using Kontakt.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Kontakt.Mapper
{
    public class KontaktMapper
    {
        public List<KontaktE> getList()
        {
            var listaKontakt = new List<KontaktE>();
            using (var conn = new SqlConnection())
            {
                conn.ConnectionString = "Data Source = 192.168.10.248; Initial Catalog = Agata; User ID = agata; Password = pbs1234";
                conn.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = conn;
                    command.CommandText = "select id, imie, nazwisko, wiek from kontakt";
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            listaKontakt.Add(new KontaktE()
                            {
                                id = (int)reader["id"],
                                imie = (string)reader["imie"],
                                nazwisko = (string)reader["nazwisko"],
                                wiek = (int)reader["wiek"]
                            });
                        }
                    }
                }
            }
            return listaKontakt;
        }
        

        public KontaktE getById(int id)
        {
            var kontakt = new KontaktE();
            using (var conn = new SqlConnection())
            {
                conn.ConnectionString = "Data Source = 192.168.10.248; Initial Catalog = Agata; User ID = agata; Password = pbs1234";
                conn.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = conn;
                    command.CommandText = $"select id, imie, nazwisko, wiek from kontakt where id = {id}";
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            kontakt.id = (int)reader["id"];
                            kontakt.imie = (string)reader["imie"];
                            kontakt.nazwisko = (string)reader["nazwisko"];
                            kontakt.wiek = (int)reader["wiek"];
                        }
                    }
                }
            }
            return kontakt;
        }
    }
}