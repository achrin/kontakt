﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Kontakt.Startup))]
namespace Kontakt
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
